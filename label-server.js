const express = require('express');
const body_parser = require('body-parser');
const fs = require('fs');
const path = require('path');

function addFrontendFile(app, finame) {
  var file = fs.readFileSync(path.join(__dirname, "frontend", finame), 'utf-8');

  let processRequests = (req, res) => {
    res.type(finame).send(file);
  };
  app.get(`/${finame}`, processRequests);
  if (finame === "index.html") {
    app.get("/", processRequests);
  }
}

class LabelServer {
  constructor(port, getFeature, putLabel) {
    var app = express();
    app.disable('etag');
    app.use(body_parser.json());
    addFrontendFile(app, "index.html");
    addFrontendFile(app, "main.js");
    addFrontendFile(app, "style.css");

    app.post("/popX", (req, res) => {
      var newFeature = getFeature();
      if (!newFeature) {
        res.json({ type: "eof" })
      }
      else {
        res.json(newFeature);
      }
    });

    app.post("/writeY", (req, res) => {
      console.log(req.body);
      putLabel(req.body.id, req.body.label);
      res.send();
    });

    app.listen(port, () => {
      console.log(`LabelServer listening on port ${port}`);
    })
  }
}

module.exports = LabelServer;
