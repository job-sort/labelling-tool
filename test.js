const LabelServer = require("./label-server");

var dataSet = (() => {
  var features = [
    "<p>Foo!</p>",
    "<p>Bar!</p>",
    "<p>Baz!</p>"
  ];

  var labels = [];

  var index = 0;
  return {
    popX: () => {
      if (index >= features.length) {
        console.log(features, labels);
        return undefined;
      }
      var sent_index = index;
      ++index;
      return {
        id: sent_index,
        type: "html",
        body: features[sent_index]
      }
    },

    putY: (id, label) => {
      labels[id] = label;
    },

    getEntries: () => {
      return {
        x: features,
        y: labels
      }
    }
  }
})();

const labelServer = new LabelServer(8080, dataSet.popX, dataSet.putY);
