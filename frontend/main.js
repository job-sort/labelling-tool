function clearElement(elem) {
  while (elem.firstChild) {
    elem.removeChild(elem.firstChild);
  }
}

function interpolateAnimFrame(f, duration, startVal, endVal, endCbk) {
  var start;
  var interpolationCall = timestamp => {
    if (!start) start = timestamp;
    var progress = (Math.min((timestamp - start), duration) / duration);
    f(progress * endVal + (1-progress) * startVal);
    if (progress < 1) {
      window.requestAnimationFrame(interpolationCall);
    }
    else if (endCbk){
      endCbk();
    }
  }
  window.requestAnimationFrame(interpolationCall);
}

function travelTime(start, finish, speed) {
  return Math.abs(start-finish)/speed;
}

function mkPost(url, jsonbody, cbk){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      cbk(xhr.responseText);
    }
  };
  xhr.open("POST", url);
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.send(JSON.stringify(jsonbody));
}


class Swiper {
  constructor(contentElement, containerElement) {
    this.contentElement = contentElement;
    this.containerElement = containerElement;
    this.isSwiping = false;
    self = this;
    window.addEventListener("keypress", (e) => {
      switch (e.key) {
        case 'a':
          self.swipeLeft();
          break;
        case 'd':
          self.swipeRight();
          break;
      }
    });

    this.fetchContent();
  }

  fetchContent() {
    var self = this;
    mkPost("popX", undefined, content => {
      content = JSON.parse(content);
      self.content_id = content.id;
      switch (content.type) {
        case "eof":
          alert("all done!");
        case "html":
          self.putHtmlContent(content.body);
        break;
        default:
          alert(`Unknown content type "${content.type}"`);
      }
    });
  }

  // TODO: untested
  putImageContent(imageUrl) {
    clearElement(this.contentElement);
    var newImg = document.createElement("img");
    newImg.src = imageUrl;
    this.contentElement.appendChild(newImg)
    this.swipePosition = 0;
  }

  putHtmlContent(html) {
    clearElement(this.contentElement);
    this.contentElement.innerHTML = html;
    this.swipePosition = 0;
  }

  sendLabel(val) {
    var self = this;
    mkPost("writeY", { id: this.content_id, label: val}, () => {
      self.fetchContent();
    });
  }

  get swipePosition() {
    return this.containerElement.scrollLeft/this.containerElement.scrollLeftMax * 2 - 1;
  }

  set swipePosition(val) {
    var scrollSpaceCoord = (val+1)/2 * this.containerElement.scrollLeftMax;
    this.containerElement.scrollLeft = scrollSpaceCoord;
  }

  swipe(target, cbk) {
    this.isSwiping = true;
    var self = this;
    var swipeStart = this.swipePosition;
    var update = p => { self.swipePosition = p }
    var duration = travelTime(swipeStart, target, 0.005)
    var atEnd = () => { self.isSwiping = false; cbk(); }

    return interpolateAnimFrame(update, duration, swipeStart, target, atEnd);
  }

  swipeLeft() {
    var self = this;
    return this.swipe(1, () => {
      self.sendLabel(false);
    });
  }

  swipeRight() {
    var self = this;
    return this.swipe(-1, () => {
      self.sendLabel(true);
    });
  }
}


var swiper;
window.onload = (e) => {
  swiper = new Swiper(document.getElementById('content'), document.getElementById('container'));
}
